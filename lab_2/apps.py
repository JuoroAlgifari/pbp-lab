from django.apps import AppConfig


class Labb2Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lab_2'
