from django.urls import path
from .views import index, add_friend

urlpatterns = [
    path('', index, name='index-lab3'),
    path('add', add_friend, name='add-lab3')
]