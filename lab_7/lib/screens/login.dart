import 'package:flutter/material.dart';
import 'home.dart';
import './signup.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({ Key? key }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String username = "Juoro";
  String password = "qwerty12345";

  final GlobalKey<FormState> _formKey =  GlobalKey<FormState>();
  TextEditingController usernameInput =  TextEditingController();
  TextEditingController passwordInput =  TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: const EdgeInsets.symmetric(
            horizontal: 30,
            vertical: 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  const Text(
                    "Login",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Silahkan Masukkan Akun Anda",
                    style: TextStyle(
                      color: Colors.grey[800],
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(
                    height:20
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 80,
                          width: 80,
                          decoration: BoxDecoration(
                            color: Colors.purple[100],
                            shape: BoxShape.circle,
                          ),
                          child: const Center(
                            child: Icon(
                              Icons.person,
                              size: 60,
                              color:Colors.purple,
                            ),
                          ),
                        ),
                      ]
                    ), 
                  ),
                  const SizedBox(
                    height: 7,
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: usernameInput,
                          validator: (value){
                            if (value == null || value.isEmpty){
                              return "Isi Username Anda";
                            } else{
                              return null;
                            }
                          },
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black,
                              ),
                            ),
                            prefixIcon: Icon(
                              Icons.person,
                              size: 40
                            ),
                            hintText: "Masukkan Username",
                            hintStyle: TextStyle(
                              color: Colors.black,
                            ),
                            labelText: "Username",
                            labelStyle: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        TextFormField(
                          controller: passwordInput,
                          validator: (value){
                            if (value == null || value.isEmpty){
                              return "Isi Password Anda";
                            } else{
                              return null;
                            }
                          },
                          obscureText: true,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            focusedBorder:  OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black,
                              ),
                            ),
                            prefixIcon: Icon(
                              Icons.lock,
                              size: 40,
                            ),
                            hintText: "Masukkan Password",
                            hintStyle: TextStyle(
                              color: Colors.black
                            ),
                            labelText: "Password",
                            labelStyle: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Column(
                          children: <Widget>[
                            MaterialButton(
                              minWidth: double.infinity,
                              height: 50,
                              onPressed: () {
                                if (_formKey.currentState!.validate()){
                                  if(usernameInput.text == username && passwordInput.text == password){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => const HomePage()
                                      ),
                                    );
                                  }
                                  else if (usernameInput.text != username){
                                    showDialog(
                                      context: context,
                                      builder: (context){
                                        return AlertDialog(
                                          title: const Text(
                                            "Login Gagal",
                                            textAlign:  TextAlign.center,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              fontSize: 22,
                                            ),
                                          ),
                                          content: const Text(
                                            "Username yang Anda masukkan salah. Silahkan coba lagi.",
                                            textAlign:  TextAlign.center,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 18,
                                            ),
                                          ),
                                          actions: [
                                            MaterialButton(
                                              minWidth: double.infinity,
                                              height: 20,
                                              onPressed: (){
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) => const LoginPage()
                                                  ),
                                                );
                                              },
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(5)
                                              ),
                                              child:const Text(
                                                "OK",
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                    ); 
                                  }
                                  else if (usernameInput.text == username && passwordInput.text != password){
                                    showDialog(
                                      context: context,
                                      builder: (context){
                                        return AlertDialog(
                                          title: const Text(
                                            "Login Gagal",
                                            textAlign:  TextAlign.center,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              fontSize: 22,
                                            ),
                                          ),
                                          content: const Text(
                                            "Password yang Anda masukkan salah. Silahkan coba lagi.",
                                            textAlign:  TextAlign.center,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 18,
                                            ),
                                          ),
                                          actions: [
                                            MaterialButton(
                                              minWidth: double.infinity,
                                              height: 20,
                                              onPressed: (){
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) => const LoginPage()
                                                  ),
                                                );
                                              },
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(5)
                                              ),
                                              child:const Text(
                                                "OK",
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                    ); 
                                  }
                                }
                              },
                              color: Colors.purple[400],
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)
                              ),
                              child:const Text(
                                "Login",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height:5,
                        ),
                        Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Text(
                                  "Belum Memiliki Akun?",
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                TextButton(
                                  child: Text(
                                    "Sign Up",
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.purple[400],
                                    ),
                                  ),
                                  onPressed: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => SignUpPage()
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          height:5,
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height/3.2,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("assets/images/login.png")
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}



