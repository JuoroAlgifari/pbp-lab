1. Terdapat beberapa perbedaan JSON dan XML, seperti :
    - JSON mendukung objek dalam tipe data primitif seperti integer, string, boolean, dan lain-lain. Sedangkan XML, mendukung tipe data kompleks, seperti bagan dan charts.
    - JSON menyimpan data seperti map yang memiliki key dan value. Sedangkan XML, menyimpan data sebagai tree structure.
    - JSON tidak mendukung penambahan komentar. Sedangkan XML, mendukung penambahan komentar.
    - JSON pengiriman datanya lebih cepat dibanding XML karena memiliki kode data yang lebih sederhana.
    - JSON lebih mudah dibaca dan dipahami dibandingkan dengan XML.
2. Terdapat beberapa perbedaan HTML dan XML, seperti :
    - HTML merupakan bahasa markup yang telah ditentukan. Sedangkan XML merupakan kerangka kerja untuk menentukan bahasa markup.
    - HTML berfokus pada penyajian data. Sedangkan XML, berfokus pada transfer data.
    - HTML tidak peka dengan huruf besar dan huruf kecil (tidak case sensitive). Sedangkan XMLL, peka dengan huruf besar dan huruf kecil (case sensitive).
    - HTML opsional menggunakan tag penutup. Sedangkan XML, wajib menggunakan tag penutup.
    - HTML memiliki tag terbatas. Sedangkan XML, tagnya dapat dikembangkan.
