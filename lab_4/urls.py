from django.urls import path
from .views import index, add_friend, note_list, note_list

urlpatterns = [
    path('', index, name='index-lab4'),
    path('add-note', add_friend, name='add-note-lab4'),
    path('note-list', note_list, name='note-list-lab4')
]